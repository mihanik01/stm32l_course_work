#include "wav.h"
#include <string.h>

const FOURCC RIFF = 0x46464952;
const FOURCC WAVE = 0x45564157;

const FOURCC SIG_FMT  = 0x20746D66;
const FOURCC SIG_DATA = 0x61746164;


const uint32_t WAV_FREQUENCY = 8000;

const uint16_t WAVE_FORMAT_PCM = 0x00001;
const uint32_t DEFAULT_SAMPLES_RATE = WAV_FREQUENCY;
const uint16_t DEFAULT_BITS_SAMPLE = 8;


const Result
	RES_OK = 0,
	RES_WAV_ERR = (1 << 0),
	RES_FMT_ERR = (1 << 1);

/// @fn init_waveformat
/// @brief Default WAVEFORMATEX initialization
void init_waveformat(WAVEFORMATEX *wave_header) {
	// Основные параметры
	wave_header->wFormatTag      = WAVE_FORMAT_PCM;
	wave_header->nChannels       = 1;                    // Моноканальный звук
	wave_header->nSamplesPerSec  = DEFAULT_SAMPLES_RATE;
	wave_header->wBitsPerSample  = DEFAULT_BITS_SAMPLE;  // 8 бит на сэмпл
	
	// Зависимые параметры
	wave_header->nBlockAlign     =
		(wave_header->nChannels * wave_header->wBitsPerSample) / 8;
		// число байт на блок (выравнивание)
	wave_header->nAvgBytesPerSec =
		wave_header->nSamplesPerSec * wave_header->nBlockAlign;
		// требуемая средняя скорость передачи данных [байт/с]
}

Result is_wav_valid(WAVEFORMATEX *wave_header) {
	return (wave_header->wFormatTag == WAVE_FORMAT_PCM &&
		wave_header->nChannels == 1 && 
		wave_header->nSamplesPerSec == DEFAULT_SAMPLES_RATE &&
		wave_header->wBitsPerSample == DEFAULT_BITS_SAMPLE) ? RES_OK : RES_WAV_ERR;
}

void init_fmt(FMT_Subchunk *fmt_subchunk) {
	fmt_subchunk->meta.ID   = SIG_FMT;
	fmt_subchunk->meta.Size = sizeof(WAVEFORMATEX);
}
