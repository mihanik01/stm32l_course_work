#ifndef WAV_H
#define WAV_H

#include <stdint.h>

typedef uint32_t FOURCC;

// стандартные сигнатуры
extern const FOURCC RIFF, WAVE;

// сигнатуры субчанков
extern const FOURCC SIG_FMT, SIG_DATA;

extern const uint32_t WAV_FREQUENCY;

typedef struct __attribute__((packed)) {
  uint16_t wFormatTag;
  uint16_t nChannels;
  uint32_t nSamplesPerSec;
  uint32_t nAvgBytesPerSec;
  uint16_t nBlockAlign;
  uint16_t wBitsPerSample;
  // uint16_t cbSize; // Ignored for WAVE_FORMAT_PCM
} WAVEFORMATEX;

typedef struct __attribute__((packed)) {
	FOURCC ID;
	uint32_t Size;
} SubchunkMeta;

typedef struct __attribute__((packed)) {
	SubchunkMeta meta;
	uint8_t      data[];
} DataSubchunk;

typedef struct __attribute((packed)) {
	SubchunkMeta meta;
} FMT_Subchunk;

typedef struct __attribute__((packed)) {
	FOURCC  formType; // тип содержимого
} RIFF_chunk;

typedef struct __attribute__((packed)) {
	FOURCC   ckID;     // сигнатура
	uint32_t ckSize;   // размер данных
} Chunk;



typedef uint32_t Result;
extern const Result RES_OK, RES_WAV_ERR, RES_FMT_ERR;
Result is_wav_valid(WAVEFORMATEX *wave_header);
void init_waveformat(WAVEFORMATEX *wave_header);

#endif
