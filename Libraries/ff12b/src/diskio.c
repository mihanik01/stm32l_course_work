/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2016        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "diskio.h"		/* FatFs lower layer API */
#include "../../STM32_SD_card/sd_main.h"
#include <stdint.h>

/* Definitions of physical drive number for each drive */
#define DEV_RAM		1	/* Example: Map Ramdisk to physical drive 0 */
#define DEV_MMC		0	/* Example: Map MMC/SD card to physical drive 1 */
#define DEV_USB		2	/* Example: Map USB MSD to physical drive 2 */

// TODO: implement different sector sizes
const uint32_t SECTOR_SIZE = 512U;

extern uint8_t  SDHC;            
extern uint8_t  CSD_STRUCTURE;
extern uint32_t C_SIZE; // [73:62] ver 1, [69:48] ver 2
extern uint8_t  C_SIZE_MULT; // [49:47] ver 1
extern uint8_t  READ_BL_LEN; // [83:80] ver 1

/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE pdrv		/* Physical drive nmuber to identify the drive */
)
{
	// DSTATUS stat;
	// int result;

	switch (pdrv) {
	case DEV_MMC :
		// result = MMC_disk_status();
		// translate the reslut code here
		// return stat;
		return 0;

	case DEV_RAM :
		// result = RAM_disk_status();
		// translate the reslut code here
		// return stat;
	case DEV_USB :
		// result = USB_disk_status();
		// translate the reslut code here
		// return stat;
		return STA_NODISK;
	}
	return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive nmuber to identify the drive */
)
{
	// DSTATUS stat;
	// int result;

	switch (pdrv) {
	case DEV_RAM :
		// result = RAM_disk_initialize();
		// translate the reslut code here
		// return stat;
	case DEV_USB :
		// result = USB_disk_initialize();
		// translate the reslut code here
		// return stat;
		return STA_NODISK | STA_NOINIT;

	case DEV_MMC :
		// result = MMC_disk_initialize();
		// translate the reslut code here
		// return stat;
		return SD_init() ? STA_NODISK : 0;
	}
	return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	DWORD sector,	/* Start sector in LBA */
	UINT count		/* Number of sectors to read */
)
{
	// DRESULT res;
	int result;
	
	switch (pdrv) {
	case DEV_RAM :
		// translate the arguments here
		// result = RAM_disk_read(buff, sector, count);
		// translate the reslut code here
		// return res;
	case DEV_USB :
		// translate the arguments here
		// result = USB_disk_read(buff, sector, count);
		// translate the reslut code here
		// return res;
		return RES_ERROR;
	
	case DEV_MMC :
		result = 0;
		while(!result && count--) {
			result = SD_ReadSector(sector++, buff);
			buff += SECTOR_SIZE;
		}
		// translate the arguments here
		// result = MMC_disk_read(buff, sector, count);
		// translate the reslut code here

		return result ? RES_ERROR : RES_OK;
	}

	return RES_PARERR;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	DWORD sector,		/* Start sector in LBA */
	UINT count			/* Number of sectors to write */
)
{
	// DRESULT res;
	int result;

	switch (pdrv) {
	case DEV_RAM :
		// translate the arguments here
		// result = RAM_disk_write(buff, sector, count);
		// translate the reslut code here
		// return res;
	case DEV_USB :
		// translate the arguments here
		// result = USB_disk_write(buff, sector, count);
		// translate the reslut code here
		// return res;

	case DEV_MMC :
		result = 0;
		while(!result && count--) {
			result = SD_WriteSector(sector++, buff);
			buff += SECTOR_SIZE;
		}
		// translate the arguments here
		// result = MMC_disk_write(buff, sector, count);
		// translate the reslut code here
		return result ? RES_ERROR : RES_OK;
	}

	return RES_PARERR;
}



/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
	switch (pdrv) {
	case DEV_RAM :
		// Process of the command for the RAM drive
		// return res;
	case DEV_USB :
		// Process of the command the USB drive
		// return res;
		return RES_ERROR;
	
	case DEV_MMC :
		// Process of the command for the MMC/SD card
		switch (cmd) {
			case CTRL_SYNC:
				return RES_OK;
			case GET_SECTOR_COUNT:
				if (CSD_STRUCTURE) {
					// Ver. 2.0
					*(DWORD*) buff = (C_SIZE + 1) * 512 * 1024 / SECTOR_SIZE; // if sector size is 512 bytes
				} else {
					// Ver. 1.0
					*(DWORD*) buff = (((C_SIZE+1) << (C_SIZE_MULT + 2)) << READ_BL_LEN) / SECTOR_SIZE; // if sector size is 512 bytes
				}
				return RES_OK;
			case GET_SECTOR_SIZE:
				*(DWORD*)buff = SECTOR_SIZE;
				return RES_OK;
			case GET_BLOCK_SIZE:
				*(DWORD*)buff = SECTOR_SIZE;
				return RES_OK;
		}
		return RES_ERROR;
	}

	return RES_PARERR;
}

