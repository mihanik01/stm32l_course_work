#include "sd_main.h"

#include <stdint.h>
#include "sd_spi.h"
#include "stm32l1xx_gpio.h"

#define GO_IDLE_STATE            0              //Программная перезагрузка   
#define SEND_IF_COND             8              //Для SDC V2 - проверка диапазона напряжений   
#define SEND_CSD                 9              // Чтение CSD
#define READ_SINGLE_BLOCK        17             //Чтение указанного блока данных  
#define WRITE_SINGLE_BLOCK       24             //Запись указанного блока данных
#define SD_SEND_OP_COND	         41             //Начало процесса инициализации  
#define APP_CMD			 55             //Главная команда из ACMD <n> команд
#define READ_OCR		 58             //Чтение регистра OCR

//глобальная переменная для определения типа карты 
uint8_t  SDHC;            
// глобальная переменная для определения версии CSD
uint8_t CSD_STRUCTURE;
// глобальная переменная для определения размера карты
uint32_t C_SIZE; // [73:62] ver 1, [69:48] ver 2
uint8_t  C_SIZE_MULT; // [49:47] ver 1
uint8_t  READ_BL_LEN; // [83:80] ver 1

//макроопределения для управления выводом SS
#define CS_ENABLE         GPIO_ResetBits(GPIOA, GPIO_Pin_15)         
#define CS_DISABLE    	  GPIO_SetBits(GPIOA, GPIO_Pin_15)
//#define CS_ENABLE 1
//#define CS_DISABLE 0
//********************************************************************************************
//function	 посылка команды в SD                                		            //
//Arguments	 команда и ее аргумент                                                      //
//return	 0xff - нет ответа   			                                    //
//********************************************************************************************
uint8_t SD_sendCommand(uint8_t cmd, uint32_t arg)
{
  uint8_t response, wait=0, tmp;
 
  //для карт памяти SD выполнить корекцию адреса, т.к. для них адресация побайтная 
  if(SDHC == 0)		
  if(cmd == READ_SINGLE_BLOCK || cmd == WRITE_SINGLE_BLOCK )  {arg = arg << 9;}
  //для SDHC корекцию адреса блока выполнять не нужно(постраничная адресация)	
 
  CS_ENABLE;
 
  //передать код команды и ее аргумент
  spi_send(cmd | 0x40);
  spi_send(arg>>24);
  spi_send(arg>>16);
  spi_send(arg>>8);
  spi_send(arg);
 
  //передать CRC (учитываем только для двух команд)
  if(cmd == SEND_IF_COND) spi_send(0x87);            
  else                    spi_send(0x95); 
 
  //ожидаем ответ
  while((response = spi_read()) == 0xff) 
   if(wait++ > 0xfe) break;                //таймаут, не получили ответ на команду
 
  //проверка ответа если посылалась команда READ_OCR
  if(response == 0x00 && cmd == 58)     
  {
    tmp = spi_read();                      //прочитат один байт регистра OCR            
    if(tmp & 0x40) SDHC = 1;               //обнаружена карта SDHC 
    else           SDHC = 0;               //обнаружена карта SD
    //прочитать три оставшихся байта регистра OCR
    spi_read(); 
    spi_read(); 
    spi_read(); 
  }
	
	// проверка ответа если посылалась команда SEND_CSD
	if (response == 0x00 && cmd == SEND_CSD)
	{
		tmp = spi_read(); // 127:120
		if (tmp & 0xC0) CSD_STRUCTURE = 1; // версия 2.0
		else            CSD_STRUCTURE = 0; // версия 1.0
		spi_read(); // 119:112
		spi_read(); // 111:104
		spi_read(); // 103:96
		
		spi_read(); // 95:88
		tmp = spi_read(); // 87:80
		READ_BL_LEN = tmp & (CSD_STRUCTURE ? 0x00 : 0x0F);
		tmp = spi_read(); // 79:72
		C_SIZE = tmp & (CSD_STRUCTURE ? 0x00 : 0x03);
		tmp = spi_read(); // 71:64
		C_SIZE <<= (CSD_STRUCTURE ? 0 : 8);
		C_SIZE |= tmp & (CSD_STRUCTURE ? 0x3F : 0xFF);
		
		tmp = spi_read(); // 63:56
		C_SIZE <<= 8;
		C_SIZE |= tmp & (CSD_STRUCTURE ? 0xFF : 0xC0);
		C_SIZE >>= (CSD_STRUCTURE ? 0 : 6);
		tmp = spi_read(); // 55:48
		C_SIZE <<= 8;
		C_SIZE |= tmp & (CSD_STRUCTURE ? 0xFF : 0x00);
		C_SIZE >>= (CSD_STRUCTURE ? 0 : 8);
		C_SIZE_MULT = tmp & (CSD_STRUCTURE ? 0x00 : 0x3);
		tmp = spi_read(); // 47:40
		C_SIZE_MULT <<= 1;
		C_SIZE_MULT |= (tmp & (CSD_STRUCTURE ? 0x00 : 0x80)) >> 7;
		spi_read(); // 39:32
		
		spi_read(); // 31:24
		spi_read(); // 23:16
		spi_read(); // 15:8
		spi_read(); // 7:0
	}

  spi_read();
 
  CS_DISABLE; 
 
  return response;
}

//********************************************************************************************
//function	 инициализация карты памяти                         			    //
//return	 0 - карта инициализирована  					            //
//********************************************************************************************
uint8_t SD_init(void)
{
  uint8_t   i;
  uint8_t   response;
  uint8_t   SD_version = 2;	          //по умолчанию версия SD = 2
  uint16_t  retry = 0 ;
 
  spi_init();                            //инициализировать модуль SPI                        
  for(i=0;i<10;i++) spi_send(0xff);      //послать свыше 74 единиц   
 
	//test
	//spi_send(0x01);
	//spi_send(0x02);
	//spi_send(0x02);
	//spi_send(0x01);
	//spi_send(0x01);
	//for(i=0;i<4;i++) spi_send(0x01);
	//for(i=0;i<4;i++) spi_send(0x03);
	//for(i=0;i<4;i++) spi_send(0x05);
	//for(i=0;i<4;i++) spi_send('*');
	//for(i=0;i<4;i++) spi_send(0x05);
	//for(i=0;i<4;i++) spi_send('*');
	//for(i=0;i<4;i++) spi_send(0x0B);
	
  //выполним программный сброс карты
  while(SD_sendCommand(GO_IDLE_STATE, 0)!=0x01)                                   
    if(retry++>0x20)  return 1;        
  spi_send (0xff);
  spi_send (0xff);
 
  retry = 0;                                     
  while(SD_sendCommand(SEND_IF_COND,0x000001AA)!=0x01)
  { 
    if(retry++>0xfe) 
    { 
      SD_version = 1;
      break;
    } 
  }
 
 retry = 0;                                     
 do
 {
   response = SD_sendCommand(APP_CMD,0); 
   response = SD_sendCommand(SD_SEND_OP_COND,0x40000000);
   retry++;
   if(retry>0xffe) return 1;                     
 }while(response != 0x00);                      
 
 
 //читаем регистр OCR, чтобы определить тип карты
 retry = 0;
 SDHC = 0;
 if (SD_version == 2)
 { 
   while(SD_sendCommand(READ_OCR,0)!=0x00)
	 if(retry++>0xfe)  break;
 }
 
 return 0; 
}

//********************************************************************************************
//function	 чтение выбранного сектора SD                         			    //
//аrguments	 номер сектора,указатель на буфер размером 512 байт                         //
//return	 0 - сектор прочитан успешно   					            //
//********************************************************************************************
uint8_t SD_ReadSector(uint32_t BlockNumb,uint8_t *buff)
{ 
  uint16_t i=0;
 
  //послать команду "чтение одного блока" с указанием его номера
  if(SD_sendCommand(READ_SINGLE_BLOCK, BlockNumb)) return 1;  
  CS_ENABLE;
  //ожидание  маркера данных
  while(spi_read() != 0xfe)                
  if(i++ > 0xfffe) {CS_DISABLE; return 1;}       
 
  //чтение 512 байт	выбранного сектора
  for(i=0; i<512; i++) *buff++ = spi_read();
 
  spi_read(); 
  spi_read(); 
  spi_read(); 
 
  CS_DISABLE;
 
  return 0;
}

//********************************************************************************************
//function	 запись выбранного сектора SD                         			    //
//аrguments	 номер сектора, указатель на данные для записи                              //
//return	 0 - сектор записан успешно   					            //
//********************************************************************************************
uint8_t SD_WriteSector(uint32_t BlockNumb, const uint8_t *buff)
{
  uint8_t     response;
  uint16_t    i,wait=0;
 
  //послать команду "запись одного блока" с указанием его номера
  if( SD_sendCommand(WRITE_SINGLE_BLOCK, BlockNumb)) return 1;
 
  CS_ENABLE;
  spi_send(0xfe);    
 
  //записать буфер сектора в карту
  for(i=0; i<512; i++) spi_send(*buff++);
 
  spi_send(0xff);                //читаем 2 байта CRC без его проверки
  spi_send(0xff);
 
  response = spi_read();
 
  if( (response & 0x1f) != 0x05) //если ошибка при приеме данных картой
  { CS_DISABLE; return 1; }
 
  //ожидаем окончание записи блока данных
  while(!spi_read())             //пока карта занята,она выдает ноль
  if(wait++ > 0xfffe){CS_DISABLE; return 1;}
 
  CS_DISABLE;
  spi_send(0xff);   
  CS_ENABLE;         
 
  while(!spi_read())               //пока карта занята,она выдает ноль
  if(wait++ > 0xfffe){CS_DISABLE; return 1;}
  CS_DISABLE;
 
  return 0;
}
