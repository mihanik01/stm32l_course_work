#include "sd_spi.h"

#include "stm32l1xx_gpio.h"
#include "stm32l1xx_spi.h"
#include "stm32l1xx_rcc.h"

//*********************************************************************************************
//function  инициализация  SPI1                                                              //
//argument  none                                                                             //
//return    none                                                                             //
//*********************************************************************************************
void spi_init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	SPI_InitTypeDef  SPI_InitStructure;
	
	// включить тактирование SPI
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
	//включить тактирование порта А
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
 
	// Конфигурируем SS
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_15;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	// Пока устанавливаем выбор карты в 1 (выключено), а потом при обращении
	// будем его изменять
	GPIO_SetBits(GPIOA, GPIO_Pin_15);
  
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	
	// Конфигурация SCK, MOSI, MISO
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_12 | GPIO_Pin_11;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	// задаём альтернативные функции пинов
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource5 , GPIO_AF_SPI1); // SCK
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource12, GPIO_AF_SPI1); // MOSI
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource11, GPIO_AF_SPI1); // MISO
	
	// TODO: configure SS pin
	
	// Конфигурация модуля SPI
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex; //полный дуплекс
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b; // передаем по 8 бит
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low; // Полярность и
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge; // фаза тактового сигнала
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft; // Управлять состоянием сигнала NSS программно
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;// SPI_BaudRatePrescaler_32; // Предделитель SCK
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB; // Первым отправляется старший бит
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master; // Режим - мастер
	
	SPI_Init(SPI1, &SPI_InitStructure); //Настраиваем SPI1
	SPI_Cmd(SPI1, ENABLE); // Включаем модуль SPI1....
	
	// Поскольку сигнал NSS контролируется программно, установим его в единицу
	// Если сбросить его в ноль, то наш SPI модуль подумает, что
	// у нас мультимастерная топология и его лишили полномочий мастера.
	SPI_NSSInternalSoftwareConfig(SPI1, SPI_NSSInternalSoft_Set);
}

//*********************************************************************************************
//function  обмен данными по SPI1                                                            //
//argument  передаваемый байт                                                                //
//return    принятый байт                                                                    //
//*********************************************************************************************
uint8_t spi_send(uint8_t data)
{
	//GPIO_ResetBits(GPIOA, GPIO_Pin_15);
		
  while (!(SPI1->SR & SPI_SR_TXE));      //убедиться, что предыдущая передача завершена
  SPI_SendData(SPI1, data);              //загружаем данные для передачи
  while (SPI_GetFlagStatus(SPI1, SPI_FLAG_BSY) == SET);
	
	//GPIO_SetBits(GPIOA, GPIO_Pin_15);
	
	while (SPI_GetFlagStatus(SPI1, SPI_FLAG_RXNE) != SET);     //ждем окончания обмена
	uint8_t res = SPI_ReceiveData(SPI1);
	
	return res; //читаем принятые данные
}

//*********************************************************************************************
//function  прием байт по SPI1                                                               //
//argument  none                                                                             //
//return    принятый байт                                                                    //
//*********************************************************************************************
uint8_t spi_read (void)
{
  return spi_send(0xff);		  //читаем принятые данные
}

