#include "record.h"

#include "ff.h"
#include "wav.h"

#include "stm32l1xx_adc.h"
#include "stm32l1xx_rcc.h"
#include "stm32l1xx_tim.h"
#include "stm32l1xx_gpio.h"
#include "stm32l1xx_dma.h"

#include "stm32_tsl_api.h"

#include "CGRAM.h"

#include <stdlib.h>
#include <stdbool.h>

static const uint8_t ALL_OK = 0, ERR_SD = 1, ERR_NOT_FOUND = 2, ERR_WRITE = 3;

// Включение АЦП
void enable_adc() {
	ADC_Cmd(ADC1, ENABLE); //И включаем ADC
  while(ADC_GetFlagStatus(ADC1, ADC_FLAG_ADONS) == RESET); //Ждем готовности ADC	
}

void disable_adc() {
	ADC_Cmd(ADC1, DISABLE);
}

void init_tim2(void) {
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
 
  /* Time base configuration */
  TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	
  TIM_TimeBaseStructure.TIM_Period = (18000000 / WAV_FREQUENCY) - 1; // 8 KHz, from 18 MHz TIM2CLK (ie APB1 = HCLK/4, TIM2CLK = HCLK/2)
  TIM_TimeBaseStructure.TIM_Prescaler = 0;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
 
  /* TIM2 TRGO selection */
  TIM_SelectOutputTrigger(TIM2, TIM_TRGOSource_Update); // ADC_ExternalTrigConv_T2_TRGO
 
  /* TIM2 enable counter */
  TIM_Cmd(TIM2, ENABLE);
}

void init_tim6(void) {
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);
	
	TIM6->PSC = (36000-1) / 2;// 1 KHz, f_APB1 = 36 MHz (???)
	TIM6->CNT = 0;
	TIM6->CR1 &= ~TIM_CR1_ARPE;
	TIM6->ARR = 20000; // 20 sec
	TIM6->CR1 &= ~TIM_CR1_OPM; // нет остановки таймера при 'update event'
	
	TIM6->DIER &= ~TIM_DIER_UIE; // запрет прерываний
}

void enable_tim6(void) {
	TIM6->SR  &= ~TIM_SR_UIF;
	TIM6->CR1 |= TIM_CR1_CEN;
	if (TIM6->SR & TIM_SR_UIF) {
		TIM6->SR  &= ~TIM_SR_UIF;
		TIM6->CR1 |= TIM_CR1_CEN;
	}
}

void disable_tim6(void) {
	TIM6->CR1 &= ~TIM_CR1_CEN;
}

void init_dma(uint8_t *buffer, uint32_t buffer_size) {
	DMA_InitTypeDef dma;
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	
	DMA_StructInit(&dma);
	
	DMA_DeInit(DMA1_Channel1);
	
	dma.DMA_MemoryBaseAddr     = (uint32_t) buffer;
	dma.DMA_MemoryDataSize     = DMA_MemoryDataSize_Byte;
	dma.DMA_PeripheralBaseAddr = (uint32_t) &ADC1->DR;
	dma.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	dma.DMA_DIR                = DMA_DIR_PeripheralSRC;
	dma.DMA_PeripheralInc      = DMA_PeripheralInc_Disable;
	dma.DMA_MemoryInc          = DMA_MemoryInc_Enable;
	dma.DMA_Priority           = DMA_Priority_VeryHigh;
	dma.DMA_Mode               = DMA_Mode_Circular;
	dma.DMA_BufferSize         = buffer_size;
	dma.DMA_M2M                = DMA_M2M_Disable;
	
	DMA_Init(DMA1_Channel1, &dma); // TODO: уточнить номер канала
	
	DMA_Cmd(DMA1_Channel1, ENABLE);
	DMA_ITConfig(DMA1_Channel1, DMA_IT_HT | DMA_IT_TC, ENABLE);
	NVIC_EnableIRQ(DMA1_Channel1_IRQn);
}

void disable_dma(void) {
	DMA_Cmd(DMA1_Channel1, DISABLE);
}

//Инициализация ADC
void init_adc(void) {
	ADC_InitTypeDef ADC_InitStruct;
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE); // Включаем тактирование порта А.
	
	GPIO_InitTypeDef gpio_adc;
	GPIO_StructInit(&gpio_adc);
	gpio_adc.GPIO_Mode = GPIO_Mode_AN; // Через РА0 - аналоговый ввод-вывод.
	gpio_adc.GPIO_PuPd = GPIO_PuPd_NOPULL; // РА0 - аналоговый ввод-вывод без подтягивания к + или -).
	gpio_adc.GPIO_Pin = GPIO_Pin_0;
	GPIO_Init(GPIOA, &gpio_adc);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE); //Включаем тактирование ADC
  
	ADC_InitStruct.ADC_Resolution  = ADC_Resolution_8b;
	ADC_InitStruct.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T2_TRGO;  // запуск преобразования по совпадинию таймера: Таймер 2, по TRGO
	ADC_InitStruct.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_Falling; // включаем запуск от внешнего события (regular group) по перепаду 1_0
	ADC_InitStruct.ADC_ScanConvMode = DISABLE;
	ADC_InitStruct.ADC_ContinuousConvMode = DISABLE;//ENABLE;
	ADC_InitStruct.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStruct.ADC_NbrOfConversion = 1;
	
	ADC_Init(ADC1, &ADC_InitStruct);  //Конифигурируем модуль ADC заданной структурой
	ADC_DMACmd(ADC1, ENABLE);
	ADC_DMARequestAfterLastTransferCmd(ADC1, ENABLE);
	// Далее делаем следующие настройки для регулярного канала: 
  // Выбор ADC - ADC1, канал № 0, число преобразований в последовательности - 1, 
  // sample time - 16 тактов
	ADC_RegularChannelConfig(ADC1, ADC_Channel_0, 1, ADC_SampleTime_16Cycles);
	
	ADC_ITConfig(ADC1, ADC_IT_EOC, DISABLE); // отключить прерывания EOC
	
	//ADC_InitStruct.ADC_Resolution = ADC_Resolution_8b;
	
	//ADC1->CR2 &= ~ADC_CR2_EXTSEL;   // запуск преобразования по совпадинию таймера
	//ADC1->CR2 |= ADC_CR2_EXTSEL & ADC_ExternalTrigConv_T2_TRGO; // Таймер 2, по TRGO
	
	//ADC1->CR2 &= ~ADC_CR2_EXTEN;   // включаем запуск от внешнего события (regular group)
	//ADC1->CR2 |= ADC_CR2_EXTEN_1;  // по перепаду 1_0
	
	//ADC1->SMPR3 &= ~ADC_SMPR3_SMP5;
	//ADC1->SMPR3 |= ADC_SMPR3_SMP5 & (ADC_SampleTime_16Cycles << (5*3)); // ставлю 16 циклов на преобразование в канале 5, где вольтметр
	
	//ADC1->SQR1 &= ~ADC_SQR1_L; // одно преобразование для regular group
	//ADC1->SQR5 = ADC_SQR5_SQ1 & ADC_Channel_5;    // выбираем 5-й канал для первого (и единственного) преобразования     (кол-во преобразования по-умолчанию 1)
  //ADC1->CR1 |= ADC_CR1_EOCIE;             // вкл. прерывания для regular group
}

const char str_zap[] = {R_Z, R_a, R_p, R_i, R_s, R_ere, 0x00};
const uint32_t 
		BUFFER_HALF = 5*1024,
		BUFFER_SIZE = BUFFER_HALF * 2;
uint8_t wav_buffer[BUFFER_SIZE];
bool can_transfer = false;

uint32_t tim_ctr = 0, tim_ctr_prev = 0xff, tim_cur = 0;
void update_tim() {
	uint16_t tim_cnt = TIM6->CNT;
	uint32_t tail = tim_cur % 20000;
	tim_cur -= tail;
	tim_cur += tim_cnt + ((tim_cnt < tail) ? 20000 : 0);
	tim_ctr = tim_cur / 1000;
}

uint8_t throwing_record_inner(uint8_t file_n, HD44780 *lcd, FATFS *fileSystem, FIL *wavFile) {
	if(f_mount(fileSystem, "0", 1) != FR_OK)
		return ERR_SD;
	
	uint8_t path[] = " .wav";
	path[0] = '0' + file_n;

	if (!(f_open(wavFile, (char*)path, FA_WRITE | FA_CREATE_ALWAYS) == FR_OK)) {
		return ERR_NOT_FOUND;
	}
	
	struct __attribute__((packed)) {
		Chunk chunk_header;
		RIFF_chunk riff_header;
		FMT_Subchunk fmt;
		WAVEFORMATEX wave;
		DataSubchunk data_header;
	} hdr;
	
	
	hdr.chunk_header.ckID = RIFF;
	hdr.chunk_header.ckSize = sizeof(hdr) - sizeof(Chunk);
	
	hdr.riff_header.formType = WAVE;
	
	hdr.fmt.meta.ID = SIG_FMT;
	hdr.fmt.meta.Size = sizeof(WAVEFORMATEX);
	
	init_waveformat(&hdr.wave);
	
	hdr.data_header.meta.ID = SIG_DATA;
	hdr.data_header.meta.Size = 0;
	
	UINT bytes_written = 0;
	f_write(wavFile, &hdr, sizeof(hdr), &bytes_written);
	if (bytes_written != sizeof(hdr)) {
		return ERR_WRITE;
	}
	
	uint32_t buffer_pos = 0;
	uint32_t data_transferred = 0;
	
	hd44780_clear(lcd);
	hd44780_move_cursor(lcd, 0, 0);
	hd44780_write_string(lcd, str_zap);
	
	init_tim2();
	init_tim6();
	init_adc();
	init_dma(wav_buffer, BUFFER_SIZE);
	bool pressed;
	bool wasUnpressed = false;
	bool stop = false;
	tim_ctr = 0;
	tim_ctr_prev = 0xff;
	tim_cur = 0;
	enable_adc();
	enable_tim6();
	
	// 2 minutes max
	while (!(stop || (tim_ctr > 120))) {
		if (tim_ctr != tim_ctr_prev) {
			hd44780_move_cursor(lcd, 0, 1);
			hd44780_write_char(lcd, '0' + (tim_ctr / 60));
			hd44780_write_char(lcd, ':');
			hd44780_write_char(lcd, '0' + (tim_ctr % 60) / 10);
			hd44780_write_char(lcd, '0' + (tim_ctr % 60) % 10);
			tim_ctr_prev = tim_ctr;
		}
			
		while(!can_transfer) {
			TSL_Action();
			// button pressed
			pressed = ((TSL_GlobalSetting.b.CHANGED && sMCKeyInfo[0].Setting.b.DETECTED && (TSLState == TSL_IDLE_STATE)));
			wasUnpressed = wasUnpressed || !pressed;
			stop = stop || (wasUnpressed && pressed);
			update_tim();
		}
		can_transfer = false;
		
		f_write(wavFile, wav_buffer + buffer_pos, BUFFER_HALF, &bytes_written);
		buffer_pos = (data_transferred += BUFFER_HALF) % BUFFER_SIZE;
		if (bytes_written != BUFFER_HALF) {
			disable_dma();
			disable_adc();
			disable_tim6();
			return ERR_WRITE;
		}
	}
	
	disable_dma();
	disable_adc();
	disable_tim6();
	
	f_lseek(wavFile, 0);
	hdr.data_header.meta.Size = data_transferred;
	hdr.chunk_header.ckSize += data_transferred;
	f_write(wavFile, &hdr, sizeof(hdr), &bytes_written);
	if (bytes_written != sizeof(hdr)) {
		return ERR_WRITE;
	}
	
	return ALL_OK;
}

void DMA1_Channel1_IRQHandler(void)
{
	//DMA_ClearITPendingBit(DMA1_IT_TC1);
	//DMA_ClearITPendingBit(DMA1_IT_HT1);
	DMA_ClearITPendingBit(DMA1_IT_GL1);
	update_tim();
	can_transfer = true;
}

static FATFS fileSystem;
static FIL wavFile;

void StartRecord(uint32_t record_n, HD44780 *lcd) {	
	uint8_t result = throwing_record_inner(record_n, lcd, &fileSystem, &wavFile);
	
	f_close(&wavFile);
	f_mount(NULL, "0", 1); // unmount
	
	// 0123456789ABCDEF
	// Ошибка записи
	const char str_err[] = {R_O, R_sh, R_i, R_b, R_k, R_a, ' ', R_z, R_a, R_p, R_i, R_s, R_i, 0x00};
	// 0123456789ABCDEF
	// Коснитесь экрана
	const char str_touch[] = {R_K, R_o, R_s, R_n, R_i, R_t, R_e, R_s, R_ere, ' ', R_ee, R_k, R_r, R_a, R_n, R_a, 0x00};
	
	if (result != ALL_OK) {
		hd44780_clear(lcd);
		hd44780_move_cursor(lcd, 0, 0);
		hd44780_write_string(lcd, str_err);
		hd44780_move_cursor(lcd, 0, 1);
		hd44780_write_string(lcd, str_touch);
		bool have_input = false;
		while (!have_input) {
			TSL_Action();
			have_input = have_input || (sMCKeyInfo[0].Setting.b.DETECTED && (TSLState == TSL_IDLE_STATE));
		}
	}
}
