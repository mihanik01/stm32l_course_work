#ifndef TSL_USER_H
#define TSL_USER_H

#include "tsl.h"

//==============================================================================
// IOs definition
//==============================================================================

// Channel IOs definition
#define CHANNEL_0_SRC    ((uint32_t)(GR11))
#define CHANNEL_0_DEST   (0)
#define CHANNEL_0_SAMPLE    (TSL_GROUP11_IO4)
#define CHANNEL_0_CHANNEL   (TSL_GROUP11_IO1)

#define CHANNEL_1_SRC    ((uint32_t)(GR11))
#define CHANNEL_1_DEST   (1)
#define CHANNEL_1_SAMPLE    (TSL_GROUP11_IO4)
#define CHANNEL_1_CHANNEL   (TSL_GROUP11_IO2)

#define CHANNEL_2_SRC    ((uint32_t)(GR11))
#define CHANNEL_2_DEST   (2)
#define CHANNEL_2_SAMPLE    (TSL_GROUP11_IO4)
#define CHANNEL_2_CHANNEL   (TSL_GROUP11_IO3)

// Banks definition
#define BANK_0_NBCHANNELS    (1)
#define BANK_0_SHIELD_SAMPLE        (TSL_GROUP6_IO4)
#define BANK_0_SHIELD_CHANNEL       (TSL_GROUP6_IO3)

#define BANK_1_NBCHANNELS    (1)
#define BANK_1_SHIELD_SAMPLE        (TSL_GROUP6_IO4)
#define BANK_1_SHIELD_CHANNEL       (TSL_GROUP6_IO3)

#define BANK_2_NBCHANNELS    (1)
#define BANK_2_SHIELD_SAMPLE        (TSL_GROUP6_IO4)
#define BANK_2_SHIELD_CHANNEL       (TSL_GROUP6_IO3)


// User Parameters
extern CONST TSL_Bank_T MyBanks[];
extern CONST TSL_TouchKeyB_T MyTKeysB[];
extern CONST TSL_Object_T MyObjects[];
extern TSL_ObjectGroup_T MyObjGroup;

void MyTKeys_ErrorStateProcess(void);
void MyTKeys_OffStateProcess(void);

void TSL_user_Init(void);
TSL_Status_enum_T TSL_user_Action(void);

#endif