#include "play.h"

#include "ff.h"
#include "wav.h"
#include "CGRAM.h"

#include "stm32_tsl_api.h"
#include "stm32l1xx_dma.h"
#include "stm32l1xx_dac.h"
#include "stm32l1xx_tim.h"
#include "stm32l1xx_gpio.h"
#include "stm32l1xx_rcc.h"

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

static const uint8_t ALL_OK = 0, ERR_TYPE = 1, ERR_NOT_FOUND = 2, ERR_SD = 3;

#define MIN(x,y) ((x)>(y) ? (y) : (x))

void init_dac() {
	/* Включаем питание ЦАП */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
	/* Включаем питание GPIOA */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	
	/* Настраиваем ногу ЦАПа */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);
 
  /* Time base configuration */
  TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
  TIM_TimeBaseStructure.TIM_Period = (18000000 / WAV_FREQUENCY) - 1; // 8 KHz, from 18 MHz TIM6CLK (ie APB1 = HCLK/4, TIM6CLK = HCLK/2)
  TIM_TimeBaseStructure.TIM_Prescaler = 0;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInit(TIM6, &TIM_TimeBaseStructure);
	
	TIM_ITConfig(TIM6, TIM_IT_Update, ENABLE);
  /* TIM6 enable counter */
  TIM_Cmd(TIM6, ENABLE);
	
	DAC_InitTypeDef dac_init;
	DAC_StructInit(&dac_init);
	
	dac_init.DAC_Trigger = DAC_Trigger_None;
	dac_init.DAC_WaveGeneration = DAC_WaveGeneration_None;
	dac_init.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
	
	DAC_Init(DAC_Channel_1, &dac_init);
	
	/* Включить DAC1 */
	DAC->CR |= DAC_CR_EN1;
}

void disable_dac() {
	TIM_Cmd(TIM6, DISABLE);
	// Выключить DAC1
	DAC->CR &= ~DAC_CR_EN1;
	// Выключаем питание ЦАП
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, DISABLE);
	NVIC_DisableIRQ(TIM6_IRQn);
}

static const uint32_t
	BUFFER_STEP = 1024,
	BUFFER_SIZE_P = BUFFER_STEP * 2;

static uint8_t wav_buffer_play[BUFFER_SIZE_P];
static size_t dac_pos, buf_size_l, buf_size_r;
static bool FLAG_EOT, FLAG_BLE, FLAG_BRE;

const uint16_t sinus[32] = {
	2047, 2447, 2831, 3185, 3498, 3750, 3939, 4056, 4095, 4056,
	3939, 3750, 3495, 3185, 2831, 2447, 2047, 1647, 1263, 909,
	599, 344, 155, 38, 0, 38, 155, 344, 599, 909, 1263, 1647
};

void TIM6_IRQHandler(void) {
	TIM_ClearFlag(TIM6, TIM_FLAG_Update);
	DAC->DHR12R1 = wav_buffer_play[dac_pos++] << 4;
	
	if (dac_pos == BUFFER_SIZE_P) {
		dac_pos = 0;
		FLAG_BRE = true;
	} else if (dac_pos == BUFFER_STEP) {
		FLAG_BLE = true;
	}
	
	if (
		(dac_pos >= BUFFER_STEP) ?
		(dac_pos == (BUFFER_STEP + buf_size_r)) :
		(dac_pos == buf_size_l)
	) {
		NVIC_DisableIRQ(TIM6_IRQn);
		FLAG_EOT = true;
	}
}

void start_play(void) {
	FLAG_BLE = FLAG_BRE = FLAG_EOT = false;
	NVIC_EnableIRQ(TIM6_IRQn);
}

uint8_t throwing_play_inner(uint8_t file_n, HD44780 *lcd, FATFS *fileSystem, FIL *wavFile) {
	if(f_mount(fileSystem, "0", 1) != FR_OK)
		return ERR_SD;
	
	uint8_t path[] = " .wav";
	path[0] = '0' + file_n;

	if (!(f_open(wavFile, (char*)path, FA_READ) == FR_OK)) {
		return ERR_NOT_FOUND;
	}
	
	uint32_t read_bytes;
	
	Chunk chunk_header;
	f_read(wavFile, &chunk_header, sizeof(Chunk), &read_bytes);
	if (read_bytes != sizeof(Chunk) || chunk_header.ckID != RIFF) {
		return ERR_TYPE;
	}
	
	RIFF_chunk riff_header;
	f_read(wavFile, &riff_header, sizeof(RIFF_chunk), &read_bytes);
	if (read_bytes != sizeof(RIFF_chunk) || riff_header.formType != WAVE) {
		return ERR_TYPE;
	}
	
	FMT_Subchunk fmt_header;
	f_read(wavFile, &fmt_header, sizeof(FMT_Subchunk), &read_bytes);
	if (read_bytes != sizeof(FMT_Subchunk) || fmt_header.meta.ID != SIG_FMT || fmt_header.meta.Size < sizeof(WAVEFORMATEX)) {
		return ERR_TYPE;
	}
	
	WAVEFORMATEX wave_info;
	f_read(wavFile, &wave_info, sizeof(WAVEFORMATEX), &read_bytes);
	if (read_bytes != sizeof(WAVEFORMATEX) || is_wav_valid(&wave_info) != RES_OK) {
		return ERR_TYPE;
	}
	
	const FSIZE_t wav_data_pos = sizeof(Chunk) + sizeof(RIFF_chunk) + sizeof(FMT_Subchunk) + fmt_header.meta.Size;
	f_lseek(wavFile, wav_data_pos);
	if (f_tell(wavFile) != wav_data_pos) {
		return ERR_TYPE;
	}
	
	DataSubchunk data_header;
	f_read(wavFile, &data_header, sizeof(DataSubchunk), &read_bytes);
	if (read_bytes != sizeof(DataSubchunk) || data_header.meta.ID != SIG_DATA) {
		return ERR_TYPE;
	}
	
	char play_mess[] = {
		R_V, R_o, R_s, R_p, R_r, R_o, R_i, R_z, R_v, R_e, R_d, R_e,
		R_n, R_i, R_e, '\0'
	};
	
	hd44780_clear(lcd);
	hd44780_move_cursor(lcd, 0, 0);
	hd44780_write_string(lcd, (const char*) play_mess);
	
	FSIZE_t wav_size = data_header.meta.Size;
	uint32_t to_read = MIN(BUFFER_SIZE_P, wav_size);
	
	f_read(wavFile, wav_buffer_play, to_read, &read_bytes);
	if (read_bytes != to_read)
		return ERR_TYPE;
	wav_size -= to_read;
	buf_size_l = MIN(read_bytes, BUFFER_STEP);
	buf_size_r = (read_bytes > BUFFER_STEP) ? (read_bytes - BUFFER_STEP) : 0;
	
	start_play();
	while (!FLAG_EOT) {
		while(!FLAG_BLE && !FLAG_BRE && !FLAG_EOT);
		if (!FLAG_EOT) {
			to_read = MIN(BUFFER_STEP, wav_size);
			f_read(wavFile, wav_buffer_play + (FLAG_BLE ? 0 : BUFFER_STEP), to_read, &read_bytes);
			if (read_bytes != to_read) {
				buf_size_l = buf_size_r = 0;
				return ERR_TYPE;
			}
			
			wav_size -= read_bytes;
			*(FLAG_BLE ? &buf_size_l : &buf_size_r) = read_bytes;
			FLAG_BLE = FLAG_BRE = false;
			
			uint32_t time_left = wav_size / WAV_FREQUENCY;
			hd44780_move_cursor(lcd, 0, 1);
			hd44780_write_char(lcd, '0' + (time_left / 60));
			hd44780_write_char(lcd, ':');
			hd44780_write_char(lcd, '0' + (time_left % 60) / 10);
			hd44780_write_char(lcd, '0' + (time_left % 60) % 10);
		}
	}
	f_close(wavFile);
	f_mount(NULL, "0", 1);
	
	return ALL_OK;
}

static FATFS fileSystem;
static FIL wavFile;

uint8_t play_inner(uint8_t file_n, HD44780 *lcd) {
	init_dac();
	
	uint8_t result = throwing_play_inner(file_n, lcd, &fileSystem, &wavFile);
	
	disable_dac();
	
	switch (result) {
		case ERR_TYPE:
			f_close(&wavFile);
		case ERR_NOT_FOUND:
			f_mount(NULL, "0", 1); // unmount
		case ERR_SD:
		default:
			return result;
	}
}

void StartPlay(uint8_t file_n, HD44780 *lcd) {
	char err_mess[17];
	
	uint8_t result = play_inner(file_n, lcd);
	
	if(result != ALL_OK) {
		//                  0123456789ABCDEF
		char* touch_mess = "Touch to skip"; 
		
		switch(result) {
			case ERR_SD:
				strcpy(err_mess, "SD Error");
				break;
			case ERR_NOT_FOUND:
				strcpy(err_mess, "File not found");
				break;
			case ERR_TYPE:
				strcpy(err_mess, "Wrong file type");
				break;
			default:
				strcpy(err_mess, "Error playing");
				break;
		}
		
		hd44780_clear(lcd);
		hd44780_move_cursor(lcd, 0, 0);
		hd44780_write_string(lcd, (const char*) err_mess);
		hd44780_move_cursor(lcd, 0, 1);
		hd44780_write_string(lcd, (const char*) touch_mess);
		
		do {
			// TSL_Action();
		} while(!TSL_GlobalSetting.b.CHANGED);
	}
}