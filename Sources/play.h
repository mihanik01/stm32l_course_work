#ifndef PLAY_H
#define PLAY_H

#include <stdint.h>
#include "hd44780.h"

void StartPlay(uint8_t file_n, HD44780 *lcd);

#endif
