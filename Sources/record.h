#ifndef RECORD_H
#define RECORD_H

#include <stdint.h>
#include "hd44780.h"

void StartRecord(uint32_t record_n, HD44780 *lcd);

#endif
