#include <stm32l1xx.h>

#include <stdlib.h>
#include <string.h>

// SPL
#include "stm32l1xx_adc.h"
#include "stm32l1xx_gpio.h"
#include "stm32l1xx_rcc.h"
#include "stm32l1xx_rtc.h"
#include "stm32l1xx_pwr.h"
#include "stm32l1xx_lcd.h"
#include "stm32l1xx_syscfg.h"
#include "stm32l1xx_tim.h"

// HD44780 by Artem Borisovskiy aka RoboCraft
#include "hd44780.h"
#include "hd44780_stm32l1xx.h"

#include "CGRAM.h"

// FatFs
#include "ff.h"

#include "stm32_tsl_api.h"
//#include "tsl.h"
//#include "tsl_user.h"

#include "play.h"
#include "record.h"

void init_lcd(void);
void delay_microseconds(uint16_t us);
uint32_t uint32_time_diff(uint32_t now, uint32_t before);
void hd44780_assert_failure_handler(const char *filename, unsigned long line);

HD44780 lcd;
HD44780_STM32L1xx_GPIO_Driver lcd_pindriver;
volatile uint32_t systick_ms = 0;

const uint16_t
	D0 = GPIO_Pin_3,
	D1 = GPIO_Pin_4,
	D2 = GPIO_Pin_5,
	D3 = GPIO_Pin_6,
	D4 = GPIO_Pin_7,
	D5 = GPIO_Pin_8,
	D6 = GPIO_Pin_9,
	D7 = GPIO_Pin_10;

const uint16_t DATA_PINS = D0 | D1 | D2 | D3 | D4 | D5 | D6| D7;

const uint16_t
	RS = GPIO_Pin_11,
	RW = GPIO_Pin_12,
	E  = GPIO_Pin_13;

GPIO_TypeDef *LCD_PORT = GPIOB;
const uint32_t LCD_RCC = RCC_AHBPeriph_GPIOB;

#define LCD_RST LCD_PORT->BSRRH
#define LCD_SET LCD_PORT->BSRRL

void lcd_port_init(){
	GPIO_InitTypeDef PORT;
	
	RCC_AHBPeriphClockCmd(LCD_RCC, ENABLE);
	
	PORT.GPIO_Pin = RS | RW | E | DATA_PINS;
	PORT.GPIO_Mode = GPIO_Mode_OUT;
	PORT.GPIO_Speed = GPIO_Speed_2MHz;
	
	GPIO_Init((GPIO_TypeDef *) LCD_PORT, &PORT);
}

void lcd_write(uint8_t data, bool command) {
	if (command) {
		LCD_RST = RS;
	} else {
		LCD_SET = RS;
	}
	
	//using 8-Bit_Mode
	
	LCD_SET = (data << D0);
	LCD_RST = (~(data << D0)) & DATA_PINS;
	
	delay_ns(100);
	LCD_SET = E;
	delay_ns(500);
	LCD_RST = E:
	
	LCD_SET = DATA_PINS; // All Data Pins High (Inactive)
}


void Delay(uint32_t nTime) {
	while(nTime--);
}

#define sizeof_arr(arr) (sizeof(arr)/sizeof((arr)[0]))

#define SLIDER_DETECTED (sMCKeyInfo[0].Setting.b.DETECTED)
#define SLIDER_POSITION (sMCKeyInfo[0].UnScaledPosition)

int main(void) {
	SysTick_Config(SystemCoreClock / 1000);
	char record_str[] = {
		R_Z, R_a, R_p, R_i, R_s, R_eri, ' ', 'N', ' ', '\0'
	};
	
	//Выбор источника тактирования SYSCLK
  RCC_HSICmd(ENABLE); //Включаем внутренний генератор HSI - 16 МГц
  while (RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET); //Ждем стабилизации HSI
  RCC_SYSCLKConfig(RCC_SYSCLKSource_HSI); //Выбираем HSI в качестве источника SYSCLK
	
	// TODO: redirect ADC to PA0
	//init_adc();
	//init_dac();
	init_lcd();
	
	//Настраиваем тач
  TSL_Init();
  sMCKeyInfo[0].Setting.b.IMPLEMENTED = 1; 
  sMCKeyInfo[0].Setting.b.ENABLED = 1;
  sMCKeyInfo[0].DxSGroup = 0x00; 
	
	//TIM_init();
	
	uint8_t record_n = 1;
	uint8_t record_prev = 0;
	
	hd44780_clear(&lcd);
	
	do {
		if (record_n != record_prev) {
			init_lcd();			
			hd44780_clear(&lcd);
			hd44780_move_cursor(&lcd, 0, 0);
			hd44780_write_string(&lcd, record_str);
			hd44780_move_cursor(&lcd, 8, 0);
			hd44780_write_char(&lcd, '0' + record_n);
		}
		record_prev = record_n;
		
		/*if (TSL_GlobalSetting.b.ENABLED == 0) {
			sMCKeyInfo[0].Setting.b.ENABLED = 1;
		} else if (TSL_GlobalSetting.b.ERROR) {
			if (sMCKeyInfo[0].Setting.b.ERROR) {
				sMCKeyInfo[0].Setting.b.ENABLED = 0;
				sMCKeyInfo[0].Setting.b.IMPLEMENTED = 1;
			}
			// sMCKeyInfo[0].Setting.b.ERROR = 0;
		}*/
		//if (sMCKeyInfo[0].State.b.DISABLED == 1) {
		//	sMCKeyInfo[0].Setting.b.ENABLED = 1;
		//	sMCKeyInfo[0].Setting.b.IMPLEMENTED = 1;
		//	sMCKeyInfo[0].Setting.b.DETECTED = 0;
		//}
		TSL_Action();
				
		//функция обработки слайдера как 4 раздельных кнопки
		//проверяем нажатие и его позицию
		if (TSL_GlobalSetting.b.CHANGED && (SLIDER_DETECTED)&&(TSLState == TSL_IDLE_STATE)) {
				uint8_t tch_pos = (SLIDER_POSITION / ((0x100 - 0x00) / 4));
				if (tch_pos > 3)
					tch_pos = 3;
				switch (tch_pos) {
					case 0:
						if (record_n > 1)
							record_n--;
						break;
					case 1:
						if (record_n < 5)
							record_n++;
						break;
					case 2:
						StartRecord(record_n, &lcd);
						record_prev = 0;
						break;
					case 3:
					default:
						StartPlay(record_n, &lcd);
						record_prev = 0;
						break;
				}
			/*if( SLIDER_POSITION <= 25 )
				//action 1;
			if( (SLIDER_POSITION > 25 ) && (SLIDER_POSITION <= 110 ))        
				//action 2;     
			if( (SLIDER_POSITION > 110 ) && (SLIDER_POSITION <= 200 ))        
				//action 3;     
			if( SLIDER_POSITION > 200 )        
				//action 4;
			*/
		} //cбрасываем нажатие
		else if((!TSL_GlobalSetting.b.CHANGED) && (TSLState == TSL_IDLE_STATE)) {
			// no changes
		}
	} while(1);
		
  /* never reached */
  // return 0;
}

void SysTick_Handler(void)
{
	++systick_ms;
}

void init_lcd(void)
{
  /* Распиновка дисплея */
  const HD44780_STM32L1xx_Pinout lcd_pinout =
  {
    {
      /* RS        */  { GPIOC, GPIO_Pin_7 },
      /* ENABLE    */  { GPIOC, GPIO_Pin_9 },
      /* RW        */  { GPIOC, GPIO_Pin_8 },
      /* Backlight */  { NULL, 0 },
      /* DP0       */  { GPIOB, GPIO_Pin_3 },
      /* DP1       */  { GPIOB, GPIO_Pin_4 },
      /* DP2       */  { GPIOB, GPIO_Pin_5 },
      /* DP3       */  { GPIOB, GPIO_Pin_6 },
      /* DP4       */  { GPIOB, GPIO_Pin_7 },
      /* DP5       */  { GPIOB, GPIO_Pin_8 },
      /* DP6       */  { GPIOB, GPIO_Pin_9 },
      /* DP7       */  { GPIOD, GPIO_Pin_2 },
    }
  };

  /* Настраиваем драйвер: указываем интерфейс драйвера (стандартный),
     указанную выше распиновку и обработчик ошибок GPIO (необязателен). */
	
  lcd_pindriver.interface = HD44780_STM32L1XX_PINDRIVER_INTERFACE;
  lcd_pindriver.pinout = lcd_pinout;
  lcd_pindriver.assert_failure_handler = hd44780_assert_failure_handler;

  /* И, наконец, создаём конфигурацию дисплея: указываем наш драйвер,
     функцию задержки, обработчик ошибок дисплея (необязателен) и опции.
     На данный момент доступны две опции - использовать или нет
     вывод RW дисплея (в последнем случае его нужно прижать к GND),
     и то же для управления подсветкой. */
  const HD44780_Config lcd_config =
  {
    (HD44780_GPIO_Interface*)&lcd_pindriver,
    delay_microseconds,
    hd44780_assert_failure_handler,
    HD44780_OPT_USE_RW
  };

  /* Ну, а теперь всё стандартно: подаём тактирование на GPIO,
     инициализируем дисплей: 16x2, 8-битный интерфейс, символы 5x8 точек. */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOD, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
  hd44780_init(&lcd, HD44780_MODE_8BIT, &lcd_config, 16, 2, HD44780_CHARSIZE_5x8);
}

void delay_microseconds(uint16_t us)
{
  SysTick->VAL = SysTick->LOAD;
  const uint32_t systick_ms_start = systick_ms;

  while (1)
  {
    uint32_t diff = uint32_time_diff(systick_ms, systick_ms_start);

    if (diff >= ((uint32_t)us / 1000) + (us % 1000 ? 1 : 0))
      break;
  }
}

uint32_t uint32_time_diff(uint32_t now, uint32_t before)
{
  return (now >= before) ? (now - before) : (UINT32_MAX - before + now);
}

void hd44780_assert_failure_handler(const char *filename, unsigned long line)
{
  (void)filename; (void)line;
  do {} while (1);
}


